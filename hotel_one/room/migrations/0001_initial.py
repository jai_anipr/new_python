# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='aboutroom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=5)),
                ('floor', models.CharField(max_length=10)),
                ('type', models.CharField(max_length=50)),
                ('price', models.CharField(max_length=10)),
                ('beds', models.CharField(max_length=20)),
            ],
        ),
    ]
