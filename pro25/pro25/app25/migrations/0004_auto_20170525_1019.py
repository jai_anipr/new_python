# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app25', '0003_auto_20170525_0958'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='country_id',
            field=models.ForeignKey(to='app25.country', null=True),
        ),
    ]
