from django.db import models

# Create your models here.

class country(models.Model):
    country = models.CharField(max_length=200)
    country_code = models.IntegerField(null=True)

    def __str__(self):
        return self.country

class city(models.Model):

    city_name = models.CharField(max_length=200)
    country_id = models.ForeignKey(country,null=True)


    def __str__(self):
        return self.city_name




