from django.shortcuts import render

# Create your views here.

from .models import city,country
from rest_framework import viewsets
from rest_framework import serializers
from .serializers import countrySerializer,citySerializer
from rest_framework import response
from .permissions import Iscustomer,Isstaff
from rest_framework import permissions,decorators


class countryviewset(viewsets.ModelViewSet):
    queryset = country.objects.all()
    serializer_class = countrySerializer
    permission_classes = (Isstaff,)

class cityviewset(viewsets.ModelViewSet):
    queryset = city.objects.all()
    serializer_class = citySerializer




    #@decorators.action(permission_classes=(AllowAny, ))
    def create(self, request, *args, **kwargs):
       # self.methods =('post',)
       # self.permission_classes = ((Iscustomer,))
        try:
            if request.data:
                city_obj = city()
                if 'city_name' in request.data:
                    city_obj.city_name =  str(request.data['city_name'])
                if 'country_id' in request.data:
                    country_id = str(request.data['country_id'])
                    country_obj = country.objects.get(id = int(country_id))
                    city.country_id = country_obj
                city_obj.save()
                return response.Response({'response': 'City added successfully'},status=201)
        except Exception,e:
            return response.Response({'Erro00000r': e.message}, status=400)

    def get_permissions(self):

        if self.request.method == 'POST':
            self.permission_classes = [Iscustomer, ]

        return super(cityviewset, self).get_permissions()


   #def get_permissions(self):
     #   if self.action in ('create'):
      #      self.permission_classes = [Iscustomer, ]
       # return super(self.__class__, self).get_permissions()