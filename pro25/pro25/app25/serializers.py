from .models import country,city
from rest_framework import serializers
class countrySerializer(serializers.ModelSerializer):
    class Meta:
        model = country
        fields = '__all__'

class citySerializer(serializers.ModelSerializer):
    class Meta:
        model = city
        fields = '__all__'





