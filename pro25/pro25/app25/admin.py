from django.contrib import admin

# Register your models here.
from .models import country,city

admin.site.register(city)
admin.site.register(country)

