# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='car_info',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10)),
                ('car_model', models.CharField(max_length=10)),
                ('year', models.CharField(max_length=10)),
                ('owner_name', models.CharField(max_length=10)),
                ('starttime', models.DateTimeField(verbose_name=b'date checkin')),
                ('repair', models.CharField(max_length=10)),
            ],
        ),
    ]
