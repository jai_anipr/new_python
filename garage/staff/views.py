from cars.models import repair_man
from cars.serializers import repair_manSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class List(APIView):
    """
    List all snippets, or create a new snippet.
    """

    def get(self, request, format=None):
        cars_all = all_cars.objects.all()
        serializer = all_carsSerializer(cars_all,many = True)
        repair_all= repair_man.objects.all()
        serializer1=repair_manSerializer(repair_all,many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = all_carsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class carsDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return all_cars.objects.get(pk=pk)
        except all_cars.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        car = self.get_object(pk)
        serializer = all_carsSerializer(car)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        car = self.get_object(pk)
        serializer = all_carsSerializer(car, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        car = self.get_object(pk)
        car.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)






















