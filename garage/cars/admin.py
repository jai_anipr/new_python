from django.contrib import admin
from cars.models import all_cars, repair_man

# Register your models here.
admin.site.register(all_cars)
admin.site.register(repair_man)