# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cars', '0004_remove_all_cars_repair_man'),
    ]

    operations = [
        migrations.AddField(
            model_name='all_cars',
            name='owner',
            field=models.ForeignKey(related_name='cars', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
