# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0002_auto_20170424_0949'),
    ]

    operations = [
        migrations.AddField(
            model_name='repair_man',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, default=1, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='repair_man',
            name='first_name',
            field=models.CharField(help_text=b'enter repair man name', max_length=100),
        ),
    ]
