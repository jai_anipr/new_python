# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='all_cars',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('car_name', models.CharField(max_length=100)),
                ('car_owner', models.CharField(max_length=100)),
                ('car_owner_num', models.CharField(max_length=100)),
                ('car_year', models.CharField(max_length=100)),
                ('car_modelname', models.CharField(max_length=100)),
                ('car_repair', models.TextField(help_text=b'Car problem', max_length=1000)),
                ('car_intime', models.DateTimeField(null=True, blank=True)),
                ('car_outtime', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='repair_man',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(help_text=b'enter repair man name', max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('phone', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='all_cars',
            name='repair_man',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='cars.repair_man', null=True),
        ),
    ]
