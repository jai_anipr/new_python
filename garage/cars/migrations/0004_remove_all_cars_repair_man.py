# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0003_auto_20170424_1105'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='all_cars',
            name='repair_man',
        ),
    ]
