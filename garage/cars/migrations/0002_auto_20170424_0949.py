# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='repair_man',
            name='id',
        ),
        migrations.AlterField(
            model_name='repair_man',
            name='first_name',
            field=models.CharField(help_text=b'enter repair man name', max_length=100, serialize=False, primary_key=True),
        ),
    ]
