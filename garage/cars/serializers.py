from rest_framework import serializers
from cars.models import all_cars,repair_man

class all_carsSerializer(serializers.ModelSerializer):

    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model   = all_cars
        fields = '__all__'


class repair_manSerializer(serializers.ModelSerializer):
    class Meta:
        model = repair_man
        fields =  '__all__'


from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):

    cars = serializers.PrimaryKeyRelatedField(many=True, queryset=all_cars.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'cars')
