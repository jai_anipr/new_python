from django.db import models

# Create your models here.

class all_cars(models.Model):
    car_name = models.CharField(max_length=100)
    car_owner = models.CharField(max_length=100)
    car_owner_num = models.CharField(max_length=100)
    car_year = models.CharField(max_length=100)
    car_modelname = models.CharField(max_length=100)
    car_repair = models.TextField(max_length=1000, help_text='Car problem')
    car_intime = models.DateTimeField(null=True,blank=True)
    car_outtime = models.DateTimeField(null=True,blank=True)
    owner = models.ForeignKey('auth.User', related_name='cars', on_delete=models.CASCADE)

    def __str__(self):
        return self.car_name + '-' + self.car_owner



class repair_man(models.Model):
    first_name=models.CharField(max_length=100,help_text='enter repair man name')
    last_name=models.CharField(max_length=100)
    date_of_birth=models.DateField(null=True,blank=True)
    email=models.EmailField(null=True,blank=True)
    phone = models.CharField(max_length=100)

    def __str__(self):
        return '%s, %s' % (self.last_name, self.first_name)


