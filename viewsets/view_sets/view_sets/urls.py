from rest_framework.routers import DefaultRouter
from view_setsapp.views import ChainViewSet, StoreViewSet, EmployeeViewSet

router = DefaultRouter()
router.register(prefix='chains', viewset=ChainViewSet)
router.register(prefix='storos', viewset=StoreViewSet)
router.register(prefix='employees', viewset=EmployeeViewSet)

from rest_framework import routers
router1 = routers.SimpleRouter

urlpatterns = router.urls
