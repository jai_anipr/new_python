#multiple assignment

a = b = c = 1
a,b,c = 1,2,"john"
print b, c

#multiple deletion
del a,b

#standard data types
# Integer  Float  String   List tuple dictionary
int1=10  long1=51924361L
Float1=.8   complex1=3.14j

#operators
#---- +,-./,%,**

#decision making, list comprehension, function
from random import randint
list=[x for x in range(100) if x%5==0 and x%3==0]

def guess():
    k=randint(0,100)
    print k
    if (k in list):#python member ship operators
        print 'got'
    else:
        print 'not'
    again=raw_input('do u want to play again').lower()
    if again=='no':
        print 'bye'
    else:
        guess()


#python identity operators

from random import randint

a= int(raw_input('enter number between 1 and : '))
b = randint(1,10)

if ( a is b ):
   print "got"
else:
   print "not"



# for loop
for number in range(100):
    if number%5==0 and number%3==0:
        print 'hello %d'%number

#while loop

count=0
while(count<9):
    print 'hello ' +  str(count)
    count+=1















